package com.rohantaneja.stackman;

import android.support.test.runner.AndroidJUnit4;
import android.test.RenamingDelegatingContext;

import com.rohantaneja.stackman.database.QuestionsDatabaseHelper;

import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getContext;

/**
 * Created by rohantaneja on 15/03/18.
 */

@RunWith(AndroidJUnit4.class)
public class DatabaseTest {

    private QuestionsDatabaseHelper questionsDatabaseHelper;

    @Test
    public void databaseTest() {
        RenamingDelegatingContext context = new RenamingDelegatingContext(getContext(), "test_");
        questionsDatabaseHelper = QuestionsDatabaseHelper.getDatabaseHelperInstance(getContext());
    }
}
