package com.rohantaneja.stackman.ui;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;

import com.rohantaneja.stackman.BaseActivity;
import com.rohantaneja.stackman.R;
import com.rohantaneja.stackman.adapter.QuestionsRecyclerViewAdapter;
import com.rohantaneja.stackman.database.QuestionsDatabaseHelper;
import com.rohantaneja.stackman.model.Question;
import com.rohantaneja.stackman.network.FetchQuestionsAsyncTask;
import com.rohantaneja.stackman.network.NetworkUtil;
import com.rohantaneja.stackman.util.Constants;
import com.rohantaneja.stackman.util.OnQuestionsFetchedListener;

import java.util.ArrayList;

public class MainActivity extends BaseActivity implements SearchView.OnQueryTextListener, OnQuestionsFetchedListener {

    private QuestionsRecyclerViewAdapter questionsAdapter;
    private RecyclerView questionsRecyclerView;
    private TextView emptyStateTextView;
    private ArrayList<Question> mQuestionsList;
    private FetchQuestionsAsyncTask asyncTask;
    private SearchView searchView;

    private boolean isSearchViewExpanded;
    private String searchQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();
    }

    private void initUI() {
        questionsRecyclerView = findViewById(R.id.questions_recycler_view);
        questionsAdapter = new QuestionsRecyclerViewAdapter(this);
        questionsRecyclerView.setAdapter(questionsAdapter);
        questionsRecyclerView.setVisibility(View.GONE);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(questionsRecyclerView.getContext(), LinearLayoutManager.VERTICAL);
        questionsRecyclerView.addItemDecoration(dividerItemDecoration);

        emptyStateTextView = findViewById(R.id.empty_state_text_view);
        emptyStateTextView.setText(getString(R.string.how_to_search_message));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        searchView = (SearchView) searchItem.getActionView();

        searchView.setQueryHint(getString(R.string.search_hint));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(this);

        searchView.setIconified(!isSearchViewExpanded);

        if (isSearchViewExpanded)
            clearSearchViewFocus();

        if (!TextUtils.isEmpty(searchQuery))
            searchView.setQuery(searchQuery, false);

        return true;
    }

    //to close keyboard after search results are displayed
    private void clearSearchViewFocus() {
        if (searchView != null)
            searchView.clearFocus();
    }

    @Override
    public boolean onQueryTextSubmit(String queryString) {

        searchQuery = queryString;
        isSearchViewExpanded = true;

        if (NetworkUtil.isNetworkAvailable(this)) {
            asyncTask = new FetchQuestionsAsyncTask(this, searchQuery, this);
            asyncTask.execute(queryString);
        } else {

            //if no internet and no data found in database for the searched query
            if (QuestionsDatabaseHelper.getDatabaseHelperInstance(this).getAllQuestions(queryString).size() == 0) {
                hideListShowEmptyView();
                emptyStateTextView.setText(getString(R.string.no_results_local));
            } else {
                //if no internet but data exists in database for the searched query
                onQuestionsFetched(QuestionsDatabaseHelper.getDatabaseHelperInstance(this).getAllQuestions(queryString));
                showToast(getString(R.string.no_internet_message));
            }
        }

        return true;
    }


    @Override
    public boolean onQueryTextChange(String queryString) {
        if (searchView != null)
            searchQuery = searchView.getQuery().toString();

        return true;
    }


    @Override
    protected void onPause() {
        if (asyncTask != null) {
            asyncTask.dismissProgressDialog();
        }
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //for persisting search query and search results on orientation change
        if (mQuestionsList != null) {
            outState.putBoolean(Constants.IS_SEARCH_VIEW_EXPANDED, !searchView.isIconified());
            outState.putString(Constants.SEARCH_VIEW_QUERY, searchView.getQuery().toString());
            outState.putParcelableArrayList(Constants.SEARCH_RESULTS_LIST, mQuestionsList);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null) {

            //restore search results
            if (savedInstanceState.getParcelableArrayList(Constants.SEARCH_RESULTS_LIST) != null) {
                mQuestionsList = savedInstanceState.getParcelableArrayList(Constants.SEARCH_RESULTS_LIST);
                questionsAdapter.updateQuestionsList(mQuestionsList);
                questionsRecyclerView.setVisibility(View.VISIBLE);
                emptyStateTextView.setVisibility(View.GONE);
            }

            //restore search query
            if (savedInstanceState.getString(Constants.SEARCH_VIEW_QUERY) != null) {
                isSearchViewExpanded = savedInstanceState.getBoolean(Constants.IS_SEARCH_VIEW_EXPANDED);
                searchQuery = savedInstanceState.getString(Constants.SEARCH_VIEW_QUERY);
            }

            clearSearchViewFocus();
        }
    }

    @Override
    public void onQuestionsFetched(ArrayList<Question> questionsList) {

        //questions data successfully fetched from API
        mQuestionsList = questionsList;
        questionsRecyclerView.setVisibility(View.VISIBLE);
        questionsAdapter.updateQuestionsList(questionsList);
        emptyStateTextView.setVisibility(View.GONE);

        clearSearchViewFocus();
    }

    @Override
    public void onError(int errorCode) {

        hideListShowEmptyView();

        switch (errorCode) {

            //error during parsing/fetching questions data
            case Constants.ERROR_NULL:
                emptyStateTextView.setText(getString(R.string.error_fetching_data_message));
                break;

            //no questions data returned by the API for the chosen query
            case Constants.ERROR_EMPTY:
                emptyStateTextView.setText(getString(R.string.no_results_api));
                break;
        }
    }

    //hide the results recycler view and display empty text view instead
    private void hideListShowEmptyView() {
        questionsRecyclerView.setVisibility(View.GONE);
        emptyStateTextView.setVisibility(View.VISIBLE);

        clearSearchViewFocus();
    }
}
