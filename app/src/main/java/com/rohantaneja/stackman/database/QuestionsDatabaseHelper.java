package com.rohantaneja.stackman.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.rohantaneja.stackman.database.QuestionsContract.QuestionsEntry;
import com.rohantaneja.stackman.model.Question;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rohantaneja on 15/03/18.
 */

public class QuestionsDatabaseHelper extends SQLiteOpenHelper {

    private static QuestionsDatabaseHelper sQuestionsDatabaseHelper;

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + QuestionsEntry.TABLE_NAME + " (" +
                    QuestionsEntry._ID + " INTEGER PRIMARY KEY," +
                    QuestionsEntry.COLUMN_NAME_QUERY + " TEXT," +
                    QuestionsEntry.COLUMN_NAME_QUESTION_ID + " INTEGER," +
                    QuestionsEntry.COLUMN_NAME_QUESTION_TITLE + " TEXT," +
                    QuestionsEntry.COLUMN_NAME_QUESTION_UPVOTES + " INTEGER)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + QuestionsEntry.TABLE_NAME;

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "QuestionsDatabase.db";

    public static QuestionsDatabaseHelper getDatabaseHelperInstance(Context context) {
        if (sQuestionsDatabaseHelper == null)
            sQuestionsDatabaseHelper = new QuestionsDatabaseHelper(context);

        return sQuestionsDatabaseHelper;
    }

    private QuestionsDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL(SQL_DELETE_ENTRIES);
        onCreate(sqLiteDatabase);
    }

    public void bulkInsertQuestions(String searchQuery, List<Question> questionList) {
        for (Question question : questionList) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(QuestionsEntry.COLUMN_NAME_QUERY, searchQuery);
            contentValues.put(QuestionsEntry.COLUMN_NAME_QUESTION_ID, question.getQuestionId());
            contentValues.put(QuestionsEntry.COLUMN_NAME_QUESTION_TITLE, question.getQuestionText());
            contentValues.put(QuestionsEntry.COLUMN_NAME_QUESTION_UPVOTES, question.getUpvotesCount());
            getWritableDatabase().insert(QuestionsEntry.TABLE_NAME, null, contentValues);
        }
    }

    public ArrayList<Question> getAllQuestions(String query) {
        ArrayList<Question> questionsList = new ArrayList<>();

        Cursor questionsCursor = getReadableDatabase().query(
                QuestionsEntry.TABLE_NAME,
                new String[]{
                        QuestionsEntry.COLUMN_NAME_QUESTION_ID,
                        QuestionsEntry.COLUMN_NAME_QUESTION_TITLE,
                        QuestionsEntry.COLUMN_NAME_QUESTION_UPVOTES
                },
                QuestionsEntry.COLUMN_NAME_QUERY + "=?", new String[]{query},
                null, null, null);

        if (questionsCursor.moveToFirst()) {
            do {
                questionsList.add(
                        new Question(
                                questionsCursor.getString(questionsCursor.getColumnIndex(QuestionsEntry.COLUMN_NAME_QUESTION_TITLE)),
                                questionsCursor.getInt(questionsCursor.getColumnIndex(QuestionsEntry.COLUMN_NAME_QUESTION_UPVOTES)),
                                questionsCursor.getInt(questionsCursor.getColumnIndex(QuestionsEntry.COLUMN_NAME_QUESTION_ID))
                        )
                );
            } while (questionsCursor.moveToNext());
        }

        questionsCursor.close();

        return questionsList;
    }

    public void deleteQuestionsWithQuery(String query) {
        getWritableDatabase().delete(
                QuestionsEntry.TABLE_NAME,
                QuestionsEntry.COLUMN_NAME_QUERY + "=?",
                new String[]{query}
        );
    }
}
