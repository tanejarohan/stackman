package com.rohantaneja.stackman.database;

import android.provider.BaseColumns;

/**
 * Created by rohantaneja on 15/03/18.
 */

public class QuestionsContract {

    //to prevent initialisation of the QuestionsContract class
    private QuestionsContract() {

    }

    public static class QuestionsEntry implements BaseColumns {
        public static final String TABLE_NAME = "questions";
        public static final String COLUMN_NAME_QUERY = "question_query";
        public static final String COLUMN_NAME_QUESTION_ID = "question_id";
        public static final String COLUMN_NAME_QUESTION_TITLE = "question_title";
        public static final String COLUMN_NAME_QUESTION_UPVOTES = "question_upvotes";
    }

}
