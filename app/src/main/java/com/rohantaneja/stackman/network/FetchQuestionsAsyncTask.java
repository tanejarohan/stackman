package com.rohantaneja.stackman.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.rohantaneja.stackman.R;
import com.rohantaneja.stackman.database.QuestionsDatabaseHelper;
import com.rohantaneja.stackman.model.Question;
import com.rohantaneja.stackman.util.Constants;
import com.rohantaneja.stackman.util.OnQuestionsFetchedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by rohantaneja on 13/03/18.
 */

public class FetchQuestionsAsyncTask extends AsyncTask<String, Void, ArrayList<Question>> {

    private ProgressDialog mProgressDialog;
    private Context mContext;
    private String mSearchQuery;
    private OnQuestionsFetchedListener mQuestionsFetchedListener;

    public FetchQuestionsAsyncTask(Context context, String searchQuery, OnQuestionsFetchedListener listener) {
        mContext = context;
        mSearchQuery = searchQuery;
        mQuestionsFetchedListener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (mContext != null) {
            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setMessage(mContext.getString(R.string.progress_dialog_message));
            mProgressDialog.show();
        }
    }

    @Override
    protected ArrayList<Question> doInBackground(String... strings) {

        BufferedReader reader = null;

        try {
            String urlString = Constants.BASE_URL + "?%20pagesize=100&order=desc&sort=relevance&q=" + strings[0] + "&site=stackoverflow&filter=!.UE46pK5nV.kfAr.";
            URL questionsURL = new URL(urlString);

            HttpURLConnection conn = (HttpURLConnection) questionsURL.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.connect();

            int statusCode = conn.getResponseCode();
            if (statusCode != 200) {
                return null;
            }

            InputStream inputStream = conn.getInputStream();
            StringBuilder stringBuilder = new StringBuilder();

            if (inputStream == null) {
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line).append("\n");
            }

            if (stringBuilder.length() == 0) {
                return null;
            }

            return deserializeResponse(stringBuilder.toString());

        } catch (Exception e) {

            e.printStackTrace();
            return null;

        } finally {

            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private ArrayList<Question> deserializeResponse(String response) {

        ArrayList<Question> questionList = new ArrayList<>();

        try {
            JSONObject responseJson = new JSONObject(response);
            JSONArray itemsJsonArray = responseJson.getJSONArray("items");

            for (int i = 0; i < itemsJsonArray.length(); i++) {
                JSONObject currentQuestionItem = itemsJsonArray.getJSONObject(i);

                int questionUpvotesCount = currentQuestionItem.getInt("up_vote_count");
                int questionId = currentQuestionItem.getInt("question_id");
                String questionText = currentQuestionItem.getString("title");

                questionList.add(new Question(questionText, questionUpvotesCount, questionId));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return questionList;
    }

    @Override
    protected void onPostExecute(ArrayList<Question> questionList) {
        super.onPostExecute(questionList);

        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();

        if (questionList == null) {
            mQuestionsFetchedListener.onError(Constants.ERROR_NULL);
        } else if (questionList.isEmpty()) {
            mQuestionsFetchedListener.onError(Constants.ERROR_EMPTY);
        } else {
            //delete previously existing data for the same query in the database to avoid duplication
            QuestionsDatabaseHelper.getDatabaseHelperInstance(mContext).deleteQuestionsWithQuery(mSearchQuery);
            //add new questions fetched from API
            QuestionsDatabaseHelper.getDatabaseHelperInstance(mContext).bulkInsertQuestions(mSearchQuery, questionList);
            mQuestionsFetchedListener.onQuestionsFetched(questionList);
        }
    }

    public void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }
}
