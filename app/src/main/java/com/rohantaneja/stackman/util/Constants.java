package com.rohantaneja.stackman.util;

/**
 * Created by rohantaneja on 13/03/18.
 */

public class Constants {

    public static final String BASE_URL = "https://api.stackexchange.com/2.2/search/advanced";

    public static final int ERROR_NULL = 99;
    public static final int ERROR_EMPTY = 98;

    public static final String SEARCH_RESULTS_LIST = "search_results_list";
    public static final String IS_SEARCH_VIEW_EXPANDED = "is_search_view_expanded";
    public static final String SEARCH_VIEW_QUERY = "search_view_query";
}
