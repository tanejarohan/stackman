package com.rohantaneja.stackman.util;

import com.rohantaneja.stackman.model.Question;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rohantaneja on 14/03/18.
 */

public interface OnQuestionsFetchedListener {

    void onQuestionsFetched(ArrayList<Question> questionList);

    void onError(int errorCode);

}
