package com.rohantaneja.stackman.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rohantaneja.stackman.R;
import com.rohantaneja.stackman.adapter.viewholder.QuestionsViewHolder;
import com.rohantaneja.stackman.model.Question;

import java.util.ArrayList;

/**
 * Created by rohantaneja on 13/03/18.
 */
public class QuestionsRecyclerViewAdapter extends RecyclerView.Adapter<QuestionsViewHolder> {

    private Context mContext;
    private ArrayList<Question> mQuestionsList;

    public QuestionsRecyclerViewAdapter(Context context) {
        mContext = context;
    }

    public void updateQuestionsList(ArrayList<Question> questionList) {
        mQuestionsList = questionList;
        notifyDataSetChanged();
    }

    @Override
    public QuestionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.question_result_item, parent, false);
        return new QuestionsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(QuestionsViewHolder holder, int position) {
        holder.bindData(mQuestionsList.get(position));
    }

    @Override
    public int getItemCount() {
        return mQuestionsList == null ? 0 : mQuestionsList.size();
    }
}