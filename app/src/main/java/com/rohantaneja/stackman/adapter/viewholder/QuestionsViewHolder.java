package com.rohantaneja.stackman.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.rohantaneja.stackman.R;
import com.rohantaneja.stackman.model.Question;

/**
 * Created by rohantaneja on 13/03/18.
 */

public class QuestionsViewHolder extends RecyclerView.ViewHolder {

    public QuestionsViewHolder(View itemView) {
        super(itemView);
    }

    public void bindData(Question questionItem) {

        //set question text
        TextView questionTextView = itemView.findViewById(R.id.question_text_view);
        questionTextView.setText(questionItem.getQuestionText());

        //set upvotes count
        TextView upvotesCountTextView = itemView.findViewById(R.id.upvote_count_text_view);
        upvotesCountTextView.setText(String.valueOf(questionItem.getUpvotesCount()));

        //change 'upvotes' to 'upvote' if number of upvotes is 1
        TextView upvotesTextView = itemView.findViewById(R.id.upvote_text_view);
        if (questionItem.getUpvotesCount() == 1)
            upvotesTextView.setText("upvote");

    }

}
