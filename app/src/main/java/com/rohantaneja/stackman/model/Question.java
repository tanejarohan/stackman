package com.rohantaneja.stackman.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by rohantaneja on 13/03/18.
 */

public class Question implements Parcelable {

    private String mQuestionText;
    private int mUpvotesCount;
    private int mQuestionId;

    public Question(String questionText, int upvotesCount, int questionId) {
        mQuestionText = questionText;
        mUpvotesCount = upvotesCount;
        mQuestionId = questionId;
    }

    protected Question(Parcel in) {
        mQuestionText = in.readString();
        mUpvotesCount = in.readInt();
        mQuestionId = in.readInt();
    }

    public static final Creator<Question> CREATOR = new Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel in) {
            return new Question(in);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };

    public String getQuestionText() {
        return mQuestionText;
    }

    public int getUpvotesCount() {
        return mUpvotesCount;
    }

    public int getQuestionId() {
        return mQuestionId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mQuestionText);
        parcel.writeInt(mUpvotesCount);
        parcel.writeInt(mQuestionId);
    }
}
